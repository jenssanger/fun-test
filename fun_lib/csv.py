"""Load CSV into a dict"""

import os

def __is_line_valid(line):
    if len(line) == 0:
        return False

    if "," not in line:
        return False
    return True

def discover_column_headers(line):
    """Convert provide line into a set of headers"""
    if not __is_line_valid(line):
        return (False, "Line is invalid")

    columns = [column.strip() for column in line.split(',')]

    header_dict = {i:columns[i] for i in range(0,len(columns)) if len(columns[i]) > 0}

    return (len(header_dict) > 0,header_dict)

def csv_line_to_dict(line, headers):
    """Convet line into a dict based on header positions"""
    if not __is_line_valid(line):
        return (False, "Line is invalid")

    columns = [column.strip() for column in line.split(',')]

    results = {}
    for i in range(0,len(columns)):
        if i in headers:
            results[headers[i]] = columns[i]

    return (len(results) > 0, results)

def csv_to_dict(file):
    """convert provided csv into a list of dict"""
    if not os.path.exists(file):
        return (False,"File not found")

    with open(file,"r") as csv_in:
        headers = None
        results = []
        for line in csv_in:
            if headers is None:
                result = discover_column_headers(line)
                if not result[0]:
                    return result
                headers = result[1]
                continue

            result = csv_line_to_dict(line,headers)
            if result[0]:
                results.append(result[1])

    return (len(results) > 0, results)
