import os
import shutil
import sys
import glob
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


version = 'x.y.z'
if os.path.exists('VERSION'):
    version = open('VERSION').read().strip()

setup(
    name='fun',
    version=version,
    description='A sample filtering system for fun',
    long_description=read('README.md'),
    long_description_content_type="text/markdown",
    packages=["fun_lib"],
    author='Jon Keatley',
    author_email='jk23@sanger.ac.uk',
    url='https://github.com/sanger-pathogens/fun',
    test_suite='pytest',
    tests_require=['pytest >= 6.1.1'],
    install_requires=[
    ],
    entry_points={'console_scripts':[
        'fun=fun_lib:main']},
    license='GPLv3',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience  :: Science/Research',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Programming Language :: Python :: 3 :: Only',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
    ],
)
