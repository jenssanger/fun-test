import argparse
import unittest
from unittest.mock import MagicMock

from fun_lib.argument_parsing import ArgumentParserBuilder


class TestCountParser(unittest.TestCase):

    def setUp(self):
        self.under_test = ArgumentParserBuilder.new_instance(lambda: ErrorRaisingArgumentParser()) \
            .with_count() \
            .build()

    def test_count(self):
        args = self.under_test.parse_args(
            ['count', '--input', 'infile', '--filter', 'country:Benin'])
        self.assertEqual(args,
                         argparse.Namespace(operation="count", input='infile', filter=['country:Benin']))

    def test_count_short_options(self):
        args = self.under_test.parse_args(
            ['count', '-i', 'infile', '-f', 'country:Benin'])
        self.assertEqual(args,
                         argparse.Namespace(operation="count", input='infile', filter=['country:Benin']))

    def test_count_input_file_missing(self):
        with self.assertRaises(ValueError) as cm:
            self.under_test.parse_args(['count', '--filter', 'country:Benin'])
        self.assertEqual(cm.exception.args[0], 'the following arguments are required: --input/-i')


class ErrorRaisingArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ValueError(message)
