import os
import shutil

import pytest

from fun_lib.csv import discover_column_headers
from fun_lib.csv import csv_line_to_dict
from fun_lib.csv import csv_to_dict

def test_disco_column_headers_empty_string_invalid():
    assert discover_column_headers("")[0] == False

def test_disco_column_headers_empty_columns_invalid():
    assert discover_column_headers(",,,,")[0] == False

def test_disco_column_headers_no_columns_invalid():
    assert discover_column_headers("no headers")[0] == False

def test_disco_column_headers_valid_header_line():
    headers = "valid,headers,line\n"
    results = discover_column_headers(headers)

    assert results[0] == True

    test_headers = headers[:-1].split(',')

    for i in range(0,len(test_headers)):
        assert test_headers[i] == results[1][i]

def test_disco_column_headers_valid_header_line_with_gap_column():
    headers = "valid,headers,,line\n"
    expected_indexes = [0,1,3]
    results = discover_column_headers(headers)

    assert results[0] == True
    assert expected_indexes == list(results[1].keys())

def test_csv_line_to_dict_valid_line():
    headers = {
        0:'test1',
        1:'test2'
    }

    line = "foo,bar"

    expect_results = {
        "test1":"foo",
        "test2":"bar"
    }

    results = csv_line_to_dict(line,headers)

    assert results[0] == True
    assert results[1] == expect_results

@pytest.fixture()
def get_test_csv():
    test_csv_path = os.path.join(os.getcwd(),"test_.csv")

    if(os.path.exists(test_csv_path)):
        os.remove(test_csv_path)

    with open(test_csv_path,"w") as test_csv_out:
        test_csv_out.write("test1,test2,,test3\n")
        test_csv_out.write("foo,bar,,foobar")

    expected_results = [{
        "test1":"foo",
        "test2":"bar",
        "test3":"foobar"
    }]

    yield test_csv_path, expected_results

    os.remove(test_csv_path)

def test_csv_to_dict_with_valid_csv(get_test_csv):
    path = get_test_csv[0]
    expected_results = get_test_csv[1]

    results = csv_to_dict(path)

    assert results[0]
    assert results[1] == expected_results

def test_csv_to_dict_with_missing_csv():
    path = os.path.join(os.getcwd(),"fake_.csv")

    results = csv_to_dict(path)

    assert results[0] == False
